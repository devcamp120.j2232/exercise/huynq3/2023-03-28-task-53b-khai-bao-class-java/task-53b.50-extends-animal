import s50.*;
public class Task53B60 {
    public static void main(String[] args) throws Exception {
        //task 6
        Animal animal1 = new Animal("Lucifer");
        Animal animal2 = new Animal("Kitty");
        System.out.println(animal1);
        System.out.println(animal2);
        //task 7
        Mammal mammal1 = new Mammal("Broklyn");
        Mammal mammal2 = new Mammal("Juliet");
        System.out.println(mammal1);
        System.out.println(mammal2);
        //task 8
        Cat cat1 = new Cat("Angel");
        Cat cat2 = new Cat("Moon");
        System.out.println(cat1);
        System.out.println(cat2);
        //task 9
        Dog dog1 = new Dog("Becky");
        Dog dog2 = new Dog("Lu");
        System.out.println(dog1);
        System.out.println(dog2);
        //task 10
        System.out.print("Greeting from " + cat1 + ": ");
        cat1.greets();
        System.out.print("Greeting from " + cat2 + ": ");
        cat2.greets();
        //task 11
        System.out.print("Greeting from " + dog1 + ": ");
        dog1.greets();
        System.out.print("Greeting from " + dog2 + ": ");
        dog2.greets(dog2);
    }
}
